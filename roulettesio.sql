CREATE DATABASE IF NOT EXISTS roulettesio;
USE roulettesio;

CREATE TABLE IF NOT EXISTS student (
    id int(11) NOT NULL AUTO_INCREMENT,
    nomfamille varchar(30) NOT NULL,
    prenom varchar(30) NOT NULL,
    classe varchar(10) NOT NULL,
    ldap tinyint(1) NOT NULL DEFAULT 0,
    bool tinyint(1) NOT NULL DEFAULT 0,
    passage int(5) NOT NULL DEFAULT 0,
    absence tinyint(1) NOT NULL DEFAULT 0,
    noteaddition int(100) DEFAULT NULL,
    notetotal int(10) DEFAULT NULL,
    average int(10) DEFAULT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

INSERT INTO `student` (`surname`, `firstname`, `class`) VALUES
        ('TEIXEIRA', 'Ryan', 'SIO2'),
        ('GOUVERNEUR', 'Théo', 'SIO2'),
        ('BOUILLON', 'Bastien', 'SIO2'),
        ('SARAZIN', 'Karen', 'SIO2'),
        ('HUREAUX', 'Samuel', 'SIO2'),
        ('BONOTTI', 'Luca', 'SIO2'),
        ('COSSE', 'Antonin', 'SIO2'),
        ('HANS', 'Matthis', 'SIO2'),
        ('SCHMIDT', 'Thomas', 'SIO2'),
        ('COLLART', 'Thibault', 'SIO2'),
        ('LAMBINET', 'Théo', 'SIO2'),
        ('SACCO', 'Mattéo', 'SIO2'),
        ('DUEZ-HENRY', 'Lucas', 'SIO2'),
        ('HUBERT', 'Léa', 'SIO2'),
        ('DECHAPPE', 'Gaëtan', 'SIO2'),
        ('PASCAL', 'Lendell', 'SIO2');

ALTER TABLE student ADD COLUMN choisi BOOLEAN NOT NULL DEFAULT 0;