<?php

include_once 'js/_bdd.php';

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>index</title>
    <link rel="stylesheet" href="css/roulette.css">
</head>

<body>
    <header>
        <div class="navbar">
            <h1 class="nav-a">Roulette - SIO</h1>
            <a href="" class="nav-a">Accueil</a>
            <a href="index.php" class="nav-a">Liste</a>
            <a href="reinitialisation_roulette.php" class="nav-a">Réinitialisation</a>
            <a href="" class="nav-a">Paramètres</a>
        </div>
    </header>
    <section>
        <form method="POST">
        
        <div class="container">
            <input type="submit" class="btn_aleatoire" value="Choisir aléatoirement" name="data_aleatoire"/>
        
            <input type="submit" class="btn_aleatoire" value="Réinitialisation des passages" name="reinit_eleve_passage"/>

            <input type="submit" class="btn_aleatoire" value="Réinitialisation des passages + notes" name="reinit_eleve_passage_note"/>
        </div>
        
        <?php
        
        echo "<table class=\"tableau_liste_eleve\"><th>Prénom</th><th>Nom</th><th>Note</th>";
        foreach ($datafeed as $item) {
            echo "<tr class=\"tr_liste_eleve\"><td class=\"td_liste_eleve\">" .$item['firstname']."</td><td class=\"td_liste_eleve\">".$item['surname']."</td><td class=\"td_liste_eleve\">" .$item['noteaddition']. "</td></tr>";
        }
        echo "</table>";
    
        // Condition permettant de tirer un étudiant aléatoirement
        if (isset($_POST['data_aleatoire'])) {
            $id='';
            echo "<table>";
            foreach ($data_aleatoire as $item) {
                $id = $item['id'];
                echo "<tr> <td>" .$item['firstname'].' '.$item['surname'].' '.$id. "</td> </tr>";
            }
            echo "</table>";
            echo "<form method=\"POST\">
                <input type=\"hidden\" name=\"id\" value=\"$id\">
                <input type=\"submit\" value=\"Absent\" class=\"btnAbs\" name=\"Absent\" />
                <input type=\"text\" class=\"data_note_input\" name=\"data_note_input\"/>
                <input type=\"submit\" value=\"Ajouter une note\" class=\"btnNote\" name=\"data_ajout_note\" />
                </form>";
        }

        if (isset($_POST['reinit_eleve_passage_note'])) {
            $matiere = $_POST['matiere'];
            $reinit_eleve_passage_note = $conn->query("UPDATE student SET passage = '0', absence = '0', note = 'NULL';");
        }
        
        if (isset($_POST['reinit_eleve_passage'])) {
            $reinit_eleve = $conn->query("UPDATE student SET passage = '0', absence = '0';");
        }
        
        if (isset($_POST['Absent'])) {
            $id = $_POST['id'];
            $data_absence_eleve = $conn->query("UPDATE student SET absence = absence + 1 WHERE id = '".$id."'");
        }

        if (isset($_POST['data_ajout_note'])) {
            $id = $_POST['id'];
            $note = mysqli_real_escape_string($conn, $_POST['data_note_input']);
            $data_note_eleve_matiere = $conn->query("UPDATE student SET noteaddition = '".$note."', passage = passage + 1 WHERE id = '".$id."'");
        }
    
        echo "<table>";
        ?>
    </section>
</body>

</html>