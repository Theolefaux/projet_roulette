<?php

require_once '_configbdd.php';

try {
    $conn = new mysqli(DB_HOST, DB_USER, DB_PWD, DB_NAME);
    mysqli_set_charset($conn, "utf8");
}
catch(Exception $e)
{
    die('Erreur : '. $e->getMessage());
}

$datafeed = $conn->query("SELECT * FROM student;");

$data_aleatoire = $conn->query("SELECT surname, firstname, id, noteaddition FROM student WHERE passage < 1 ORDER BY RAND (NOW()) LIMIT 1;");

?>