<?php

    include_once 'js/_bdd.php';
    
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>index</title>
    <link rel="stylesheet" href="css/roulette.css">
    <script src="js/suppression.js"></script>
</head>

<body>
    <header>
        <div class="navbar">
            <h1 class="nav-a">Roulette - SIO</h1>
            <a href="" class="nav-a">Accueil</a>
            <a href="index.php" class="nav-a">Liste</a>
            <a href="reinitialisation_roulette.php" class="nav-a">Réinitialisation</a>
            <a href="" class="nav-a">Paramètres</a>
        </div>
    </header>
    <section>
        <!-- Demande de réinitialisation étudiant -->
        <p>Voulez-vous réinitialiser le(s) passage(s) des étudiants ?</p>
        <button onclick="ouvrirPopup()">Supprimer</button>
        <!-- Pop-up confirmation suppresion -->
        <div id="ma-popup" class="popup">
            <p>Êtes-vous sûr de vouloir supprimer ?</p>
            <button onclick="confirmerSuppression()">Oui</button>
            <button onclick="fermerPopup()">Non</button>
        </div>

        <!-- Demande de réinitialisation étudiant + note -->
        <p>Voulez-vous réinitialiser le(s) passage(s) des étudiants + les notes ?</p>
        <button class="btn-reinitialisation-note">Supprimer</button>
        <!-- Pop-up confirmation suppresion -->
        <div id="ma-popup" class="popup">
            <p>Êtes-vous sûr de vouloir supprimer ?</p>
            <button onclick="confirmerSuppression()">Oui</button>
            <button onclick="fermerPopup()">Non</button>
        </div>
    </section>
</body>

</html>